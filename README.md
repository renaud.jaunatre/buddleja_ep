# Buddleja_EP

Data and code for the "Early competition by a local species mixture limits invasion by *Buddleja davidii* Franch." paper (in prep).